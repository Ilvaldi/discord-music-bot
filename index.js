const MusicBot = require('./src');
const { token, adminUserId } = require('./auth.json');

const config = {
  // these values are always required.
  token,

  // permissions is technically optional, but if you want to access to all
  // permissions you'll need to at the very least make yourself an admin.
  permissions: {
    users: {
      'YOUR USER ID': 'admin',
    },
  },
};

config.permissions.users[adminUserId] = 'admin';

const musicBot = new MusicBot(config);

musicBot.run();
