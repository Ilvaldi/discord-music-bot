const format = require('string-format');

const SELECT_GROUP = 'selectCommand';

/**
 * Now Playing command.
 *
 * Replies with the currently playing song.
 *
 * @param {object} musicbot - The musicbot.
 * @param {object} msg      - The message object that called the command.
 * @param {string[]} args   - List of arguments.
 */
const run = function run(musicbot, msg, args) { // eslint-disable-line
  const selection = args[0] - 1;
  const searchResults = musicbot.getSearchResults();

  if (searchResults.length === 0) {
    msg.reply(musicbot.getReplyMsg(SELECT_GROUP, 'noSearchResults'));
  } else if (selection >= 0 && selection < searchResults.length) {
    musicbot.queueYoutubeVideo(msg, searchResults[selection]);
  } else {
    msg.reply(format(musicbot.getReplyMsg(SELECT_GROUP, 'invalidSelection'), 1, searchResults.length));
  }
};

const info = {
  name: 'Select',
  aliases: ['select', 'sel', 'pick', 'pk'],
  usage: 'select <option[0...N]>',
  description: 'Select a search result to add to playlist.',
  permission: 'search',
};

module.exports = {
  info,
  run,
};
