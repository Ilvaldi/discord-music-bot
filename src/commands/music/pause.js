const PAUSE_GROUP = 'pauseCommand';

/**
 * Pause command.
 *
 * Pauses any currently playing song.
 *
 * @param {object} musicbot - The musicbot.
 * @param {object} msg      - The message object that called the command.
 * @param {string[]} args   - List of arguments.
 */
const run = function run(musicbot, msg, args) { // eslint-disable-line
  if (musicbot.isVoiceHandlerSet() && !musicbot.isPlaybackPaused()) {
    musicbot.voiceHandler.pause();
    musicbot.setPlaybackPaused(true);
    musicbot.setBotNowPlaying(`${musicbot.nowPlaying.title} (Paused)`);
    msg.reply('Pausing!');
  } else {
    msg.reply(musicbot.getReplyMsg(PAUSE_GROUP, 'notPlaying'));
  }
};

const info = {
  name: 'Pause',
  aliases: ['pause'],
  usage: 'pause',
  description: 'Pauses any currently playing song.',
  permission: 'pause',
};

module.exports = {
  info,
  run,
};
