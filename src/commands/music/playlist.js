const PLAYLIST_GROUP = 'playlistCommand';

/**
 * Formats song details for playlist display
 *
 * @param {string} item - The song details to format
 * @returns {string}    - The formatted string for display
 */
const itemToString = (item, index) =>
  `\`${index}\` **${item.title}** (requested by ${item.requestedBy})`;

/**
 * Playlist command.
 *
 * Replies with the current playlist for the bot.
 *
 * @param {object} musicbot - The musicbot.
 * @param {object} msg      - The message object that called the command.
 * @param {string[]} args   - List of arguments.
 */
const run = function run(musicbot, msg, args) { // eslint-disable-line
  if (!musicbot.isQueueEmpty()) {
    let playlistMessage = musicbot.getReplyMsg(PLAYLIST_GROUP, 'currentPlaylist');
    playlistMessage += musicbot.playlistQueue.map((item, index) => itemToString(item, index)).join('\n');
    const split = playlistMessage.length > 2000;

    msg.channel.send(playlistMessage, { split });
  } else {
    msg.reply(musicbot.getReplyMsg(PLAYLIST_GROUP, 'queueIsEmpty'));
  }
};

const info = {
  name: 'Playlist',
  aliases: ['playlist', 'queue'],
  usage: 'playlist',
  description: 'Shows the current playlist for the bot.',
  permission: 'playlist',
};

module.exports = {
  info,
  run,
};
