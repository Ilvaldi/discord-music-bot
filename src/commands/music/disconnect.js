/**
 * Disconnect command.
 *
 * Disconnects the music bot from it's current voice channel.
 *
 * @param {object} musicbot - The musicbot.
 * @param {object} msg      - The message object that called the command.
 * @param {string[]}  args     - List of arguments.
 */
const run = function run(musicbot, msg, args) { // eslint-disable-line
  musicbot.activeVoiceChannel.leave();
  musicbot.resetBotState();
};

const info = {
  name: 'Disconnect',
  aliases: ['disconnect', 'd', 'leave'],
  usage: 'disconnect',
  description: 'Disconnects the bot from it\'s current voice channel.',
  permission: 'disconnect',
};

module.exports = {
  info,
  run,
};
