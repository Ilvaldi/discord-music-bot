const STOP_GROUP = 'pauseCommand';

/**
 * Stop command.
 *
 * Stops the current playlist and skips the current song.
 *
 * @param {object} musicbot - The musicbot.
 * @param {object} msg      - The message object that called the command.
 * @param {string[]} args   - List of arguments.
 */
const run = function run(musicbot, msg, args) { // eslint-disable-line
  if (musicbot.isPlaybackStopped()) {
    msg.reply(musicbot.getReplyMsg(STOP_GROUP, 'alreadyStopped'));
  } else {
    musicbot.setPlaybackStopped(true);
    if (musicbot.voiceHandler !== null) {
      musicbot.voiceHandler.end();
    }
    musicbot.setBotNowPlaying(null);
    msg.reply('Stopping!');
  }
};

const info = {
  name: 'Stop',
  aliases: ['stop', 'st'],
  usage: 'stop',
  description: 'Stops the current playlist and skips the current song.',
  permission: 'stop',
};

module.exports = {
  info,
  run,
};
