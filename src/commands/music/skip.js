const SKIP_GROUP = 'skipCommand';

/**
 * Skip command.
 *
 * Skips the currently playing song.
 *
 * @param {object} musicbot - The musicbot.
 * @param {object} msg      - The message object that called the command.
 * @param {string[]} args   - List of arguments.
 */
const run = function run(musicbot, msg, args) { // eslint-disable-line
  if (musicbot.isVoiceHandlerSet()) {
    msg.reply(musicbot.getReplyMsg(SKIP_GROUP, 'skippingSong'));
    musicbot.voiceHandler.end();
  } else {
    msg.reply(musicbot.getReplyMsg(SKIP_GROUP, 'nothingPlaying'));
  }
};

const info = {
  name: 'Skip',
  aliases: ['skip', 'sk', 'next', 'n'],
  usage: 'skip',
  description: 'Skips the current song.',
  permission: 'skip',
};

module.exports = {
  info,
  run,
};
