const util = require('../../util/util.js');

const SEARCH_GROUP = 'searchCommand';

/**
 * Now Playing command.
 *
 * Replies with the currently playing song.
 *
 * @param {object} musicbot - The musicbot.
 * @param {object} msg      - The message object that called the command.
 * @param {string[]} args   - List of arguments.
 */
const run = function run(musicbot, msg, args) { // eslint-disable-line
  util.getYoutubeSearchResults(msg.author.username, args)
    .then((searchResults) => {
      if (!searchResults || searchResults.length === 0) {
        msg.reply(musicbot.getReplyMsg(SEARCH_GROUP, 'noResults'));
      }

      musicbot.setSearchResults(searchResults);

      searchResults.forEach((searchResult, index) => {
        const embed = util.getSearchResultEmbed(searchResult, index);
        msg.channel.send(embed);
      });
    });
};

const info = {
  name: 'Search',
  aliases: ['search', 'sea', 'find', 'f'],
  usage: 'search <term0> <term1> ...<termN>',
  description: 'Search for videos on Youtube.',
  permission: 'search',
};

module.exports = {
  info,
  run,
};
