// Music commands
const DISCONNECT_COMMAND = require('./music/disconnect');
const NOW_PLAYING_COMMAND = require('./music/nowplaying');
const PAUSE_COMMAND = require('./music/pause');
const PLAY_COMMAND = require('./music/play');
const PLAYLIST_COMMAND = require('./music/playlist');
const RESUME_COMMAND = require('./music/resume');
const SEARCH_COMMAND = require('./music/search');
const SELECT_COMMAND = require('./music/select');
const SKIP_COMMAND = require('./music/skip');
const STOP_COMMAND = require('./music/stop');
const SUMMON_COMMAND = require('./music/summon');

// Utility commands
const HELP_COMMAND = require('./utility/help.js');
const SET_USERNAME_COMMAND = require('./utility/setusername.js');
const SET_AVATAR_COMMAND = require('./utility/setavatar.js');

// Note: The export order here matters as it defines the command search order and the display of
// the !help command.
module.exports = [
  // Music commands
  DISCONNECT_COMMAND,
  NOW_PLAYING_COMMAND,
  PAUSE_COMMAND,
  PLAY_COMMAND,
  PLAYLIST_COMMAND,
  RESUME_COMMAND,
  SEARCH_COMMAND,
  SELECT_COMMAND,
  SKIP_COMMAND,
  STOP_COMMAND,
  SUMMON_COMMAND,

  // Utility commands
  HELP_COMMAND,
  SET_USERNAME_COMMAND,
  SET_AVATAR_COMMAND,
];
