const { RichEmbed } = require('discord.js');
const axios = require('axios');
const moment = require('moment');
const momentDurationFormatSetup = require('moment-duration-format');
const { youtubeApiKey } = require('../../auth.json');
const { maxYoutubeSearchResults } = require('../config/settings');

momentDurationFormatSetup(moment);

/**
 * Get the Youtube video url given a video id.
 *
 * @param  {string} videoId - The id of the video
 * @return {string}         - The url of the associated video
*/
const getYoutubeVideoUrl = videoId => `https://www.youtube.com/watch?v=${videoId}`;

/**
 * Convert ISO 8601 duration to HH:mm:ss timestamp.
 *
 * @param  {string} duration - The id of the video
 * @return {string}          - The url of the video
*/
const getFormattedTimestamp = duration => moment.duration(duration).format('HH:mm:ss', { trim: false });

/**
 * Given a url, will return the Youtube video id.
 *
 * Looks for the following matches:
 *  youtu.be/<id>
 *  ?v=<id>
 *
 * @param  {string} url  - The url to extract the id from.
 * @return {string | null} - The first instance of the id extracted from the url or null if none was
 *                           found.
 */
const getYoutubeVideoId = (url) => {
  if (/youtu\.?be/.test(url)) {
    const videoId = /(youtube\.com\/watch\?v=|youtu\.be\/)([a-zA-Z0-9_-]+)/gi.exec(url);
    return (videoId && videoId.length > 0) ? videoId[2] : null;
  }

  return null;
};

/**
 * Given a url, will return the Youtube playlist id.
 *
 * Looks for the following matches:
 *  ?list=<id>
 *  &list=<id>
 *
 * @param  {string} url    - The url to extract the id from.
 * @return {string | null} - The first instance of the id extracted from the url or null if none was
 *                           found.
 */
const getYoutubePlaylistId = (url) => {
  if (/youtu\.?be/.test(url)) {
    const playlistId = /[&|\?]list=([a-zA-Z0-9_-]+)/gi.exec(url);
    return (playlistId && playlistId.length > 0) ? playlistId[1] : null;
  }

  return null;
};

/**
 * Convert a item from Youtube API to a object representing the video details.
 *
 * @param {string} requestor - The user who requested the Youtube video.
 * @param {object} item      - The item representing a video returned by Youtube API
 */
const createYoutubeVideoDetails = (requestor, item) => {
  const { id, snippet, contentDetails } = item;
  return {
    title: snippet.title,
    image: snippet.thumbnails.default.url,
    url: getYoutubeVideoUrl(id),
    duration: getFormattedTimestamp(contentDetails.duration),
    requestedBy: requestor,
    source: 'Youtube',
    sourceImage: 'https://i.imgur.com/nZ5aw5i.png',
  };
};

/**
 * Get the details of Youtube video by id and return it in a special object for the bot.
 *
 * @param  {string} requestor - The user who requested the Youtube video.
 * @param  {string} videoId   - The id of the video to get with googleapis.
 * @return {object}           - After resolving a promise, returns an object with the video's title,
 *                              image, url, duration (in HH:mm:ss timestamp), requestedBy, source
 *                              and sourceImage.
 */
const getYoutubeVideoDetails = (requestor, videoId) => new Promise((resolve) => {
  axios.get('https://www.googleapis.com/youtube/v3/videos', {
    params: {
      part: 'snippet,contentDetails',
      id: videoId,
      key: youtubeApiKey,
    },
  })
    .then(res => resolve(createYoutubeVideoDetails(requestor, res.data.items[0])));
});

/**
 * Get the details of Youtube video by id and return it in a list of special objects for the bot.
 *
 * @param  {string} requestor  - The user who requested the Youtube video.
 * @param  {string[]} videoIds - List of the ids of the videos to get with googleapis.
 * @return {object}            - After resolving a promise, returns a list of objects with the
 *                               video's title, image, url, duration (in HH:mm:ss timestamp),
 *                               requestedBy, source and sourceImage.
 */
const getYoutubeVideoDetailsList = (requestor, videoIds) => new Promise((resolve) => {
  axios.get('https://www.googleapis.com/youtube/v3/videos', {
    params: {
      part: 'snippet,contentDetails',
      id: videoIds.join(','),
      key: youtubeApiKey,
    },
  })
    .then((res) => {
      const { items } = res.data;
      const videoDetailsList = items.map(item => createYoutubeVideoDetails(requestor, item));
      resolve(videoDetailsList);
    });
});

/**
 * Get the results from searching Youtube with a provided terms.
 *
 * @param {string[]} terms - The search terms to find relevant videos.
 */
const getYoutubeSearchResults = (requestor, terms) => new Promise((resolve) => {
  axios.get('https://www.googleapis.com/youtube/v3/search', {
    params: {
      part: 'snippet',
      q: terms.join(','),
      type: 'video',
      maxResults: maxYoutubeSearchResults,
      key: youtubeApiKey,
    },
  })
    .then((res) => {
      const { items } = res.data;
      const videoIds = items.map(item => item.id.videoId);
      resolve(getYoutubeVideoDetailsList(requestor, videoIds));
    });
});

/**
 * Get the details of Youtube videos in playlist by id and return it in a list of special objects
 * for the bot.
 *
 * @param {string[]} playlistId - The id of the playlist to get with googleapis.
 */
const getYoutubePlaylistVideoDetails = (requestor, playlistId, pageToken = null) =>
  new Promise((resolve) => {
    axios.get('https://www.googleapis.com/youtube/v3/playlistItems', {
      params: {
        part: 'snippet',
        playlistId,
        key: youtubeApiKey,
        maxResults: 50,
        pageToken,
      },
    })
      .then((res) => {
        const { nextPageToken, items } = res.data;
        const videoIds = items.map(item => item.snippet.resourceId.videoId);
        getYoutubeVideoDetailsList(requestor, videoIds)
          .then((videoDetailsList) => {
            if (nextPageToken) {
              getYoutubePlaylistVideoDetails(requestor, playlistId, nextPageToken)
                .then((nextVideoDetailsList) => {
                  videoDetailsList.push(...nextVideoDetailsList);
                  resolve(videoDetailsList);
                });
            } else {
              resolve(videoDetailsList);
            }
          });
      });
  });

/**
 * Returns a RichEmbed of the now playing song.
 *
 * @param  {string} title       - Song title.
 * @param  {string} image       - Song image.
 * @param  {string} url         - Url of song source.
 * @param  {string} duration    - Duration of song in seconds.
 * @param  {string} requestedBy - User that requested the song.
 * @param  {string} source      - Playback source.
 * @param  {string} sourceImage - Playback source logo/image.
 * @return {RichEmbed}          - The embed to be sent back to the activeTextChannel.
 */
const getNowPlayingEmbed = ({
  title,
  image,
  url,
  duration,
  requestedBy,
  source,
  sourceImage,
}) =>
  new RichEmbed()
    .setAuthor(`Now Playing (via ${source})`, sourceImage)
    .setTitle(title)
    .setDescription(`Length: ${duration}`)
    .setImage(image)
    .setURL(url)
    .setFooter(`Requested by ${requestedBy}`);

/**
 * Returns a RichEmbed of the search result.
 *
 * @param  {string} title       - Song title.
 * @param  {string} image       - Song image.
 * @param  {string} url         - Url of song source.
 * @param  {string} sourceImage - Playback source logo/image.
 * @return {RichEmbed}          - The embed to be sent back to the activeTextChannel.
 */
const getSearchResultEmbed = ({
  title,
  image,
  url,
  duration,
  sourceImage,
}, index) =>
  new RichEmbed()
    .setAuthor(`[${index + 1}] ${title}`, sourceImage)
    .setTitle(title)
    .setDescription(`Length: ${duration}`)
    .setImage(image)
    .setURL(url);

module.exports = {
  getYoutubeVideoDetails,
  getYoutubeVideoId,
  getYoutubePlaylistId,
  getYoutubeSearchResults,
  getYoutubePlaylistVideoDetails,
  getNowPlayingEmbed,
  getSearchResultEmbed,
};
