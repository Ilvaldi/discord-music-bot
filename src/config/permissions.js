module.exports = {
  global: {
    nowplaying: true,
    disconnect: true,
    pause: true,
    play: true,
    playlist: true,
    resume: true,
    search: true,
    skip: true,
    stop: true,
    summon: true,
    help: true,
    setavatar: false,
    setusername: false,
  },
  groups: {
    admin: {
      setavatar: true,
      setusername: true,
    },
  },
};
