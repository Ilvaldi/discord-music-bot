/**
 * Messages that are used in replies on to Discord users who interact with the bot.
 */
module.exports = {
  general: {
    // reply + error
    couldntConnect: 'I couldn\'t connect to your voice channel.',
    // reply
    notInVoiceChannel: 'You need to be in a voice channel.',
    // reply
    unknownCommand: 'Hmmm. I couldn\'t find that command... did you mistype it?',
    // reply
    noPermission: 'It would seem that you don\'t have permission for that command. :fearful:',
    // message | @user | help command
    mentionedMessage: 'Hey {}, you should try `{}` for a list of commands. :thumbsup:',
    // message
    queueEmpty: 'Looks like your playlist is empty. :sob:',
    // message | video title
    youtubeVideoAdded: 'Added {} to the queue.',
  },

  summonCommand: {},

  disconnectCommand: {},

  nowPlayingCommand: {
    // reply
    nothingPlaying: 'There\'s nothing playing right now.',
  },

  pauseCommand: {
    // reply
    notPlaying: 'I can\'t pause when I\'m not playing :thinking:',
  },

  playCommand: {
    // reply | help command
    unknownPlayUrl: 'I didn\'t understand that URL.',
  },

  playlistCommand: {
    // reply
    queueIsEmpty: 'It would seem the playlist is empty. :sob:',
    // message
    currentPlaylist: 'Here\'s the current playlist :notes:\n',
  },

  resumeCommand: {
    // reply
    alreadyPlaying: 'I\'m already playing though! :wink:',
  },

  searchCommand: {
    // reply
    noResults: 'I could not find any videos with the provided terms.',
  },

  selectCommand: {
    // reply
    noSearchResults: 'You have to first search for videos before selecting one to play.',
    // reply
    invalidSelection: 'Please choose an option between `{}-{}`.',
  },

  skipCommand: {
    // reply
    skippingSong: 'Skipping song',
    // reply
    nothingPlaying: 'I can\'t skip when not playing',
  },

  stopCommand: {
    // reply
    alreadyStopped: 'But I\'ve already stopped playing... :sweat:',
  },

  helpCommand: {
    // reply | help command
    unknown: 'I can\'t see a command or alias for that one... why don\'t you try `{}`?',
    // reply
    hereYouAre: 'Here\'s everything I could find...',
  },

  setUsernameCommand: {
    // reply
    success: ':ok_hand: Username successfully set!',
    // reply + error
    error: 'Unable to set username',
    // reply
    invalidUsername: 'Uhh... that doesn\'t seem to be something I could name myself...',
  },

  setAvatarCommand: {
    // reply
    success: ':ok_hand: Avatar successfully set!',
    // reply + error
    error: 'Unable to set avatar',
    // reply
    invalidUrl: 'Are you sure that\'s a valid URL..?',
  },
};
